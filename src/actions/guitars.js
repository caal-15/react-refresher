import definitions from '../definitions/guitars'
import uuidv4 from 'uuid/v4'

export const addGuitar = (guitar) => {
  guitar.id = uuidv4()
  return {
    type: definitions.ADD_GUITAR,
    guitar: guitar
  }
}

export const deleteGuitar = (id) => {
  return {
    type: definitions.DELETE_GUITAR,
    id: id
  }
}