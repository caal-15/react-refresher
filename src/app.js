import React from 'react'
import GuitarForm from './components/guitarForm'
import GuitarList from './components/guitarList'

const App = () => {
  return (
    <div>
      <GuitarForm />
      <GuitarList />
    </div>
  )
}

export default App