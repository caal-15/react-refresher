const definitions = {
  ADD_GUITAR: 'ADD_GUITAR',
  DELETE_GUITAR: 'DELETE_GUITAR'
}

export default definitions