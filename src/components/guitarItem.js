import React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  onDelete: PropTypes.func.isRequired,
  guitar: PropTypes.shape({
    id: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired
  })
}

const GuitarItem = ({ guitar, onDelete }) => {
  return (
    <tr>
      <td>{ guitar.model }</td>
      <td>{ guitar.brand }</td>
      <td>
        <button onClick={onDelete(guitar.id)}>Delete</button>
      </td>
    </tr>
  )
}

GuitarItem.propTypes = propTypes

export default GuitarItem