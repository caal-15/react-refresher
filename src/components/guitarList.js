import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { deleteGuitar } from '../actions/guitars'
import GuitarItem from './guitarItem'

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  guitar: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired
  }))
}

const GuitarList = ({ dispatch, guitars }) => {
  const onDelete = (id) => {
    return () => {
      dispatch(deleteGuitar(id))
    }
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Model</th>
          <th>Brand</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {guitars.map((guitar) => {
          return (
            <GuitarItem guitar={guitar} onDelete={onDelete} key={guitar.id} />
          )
        })}
      </tbody>
    </table>
  )
}

GuitarList.propTypes = propTypes

const mapStateToProps = (state) => {
  return { guitars: state.guitars }
}

export default connect(mapStateToProps)(GuitarList)