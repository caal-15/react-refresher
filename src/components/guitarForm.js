import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addGuitar } from '../actions/guitars'


const propTypes = {
  dispatch: PropTypes.func.isRequired
}

const GuitarForm = ({ dispatch }) => {
  let model
  let brand

  const compGuitar = (e) => {
    e.preventDefault()
    if (model.value && brand.value) {
      dispatch(addGuitar({
        model: model.value,
        brand: brand.value
      }))
      model.value = ''
      brand.value = ''
    } else {
      alert('Specify all fields')
    }
  }

  return (
    <form onSubmit={ compGuitar }>
      <input type='text' placeholder='Model' ref={ (c) => model = c } />
      <input type='text' placeholder='Brand' ref={ (c) => brand = c } />
      <button type='submit'>Add Guitar</button>
    </form>
  )
}

GuitarForm.propTypes = propTypes

export default connect()(GuitarForm)