import definitions from '../definitions/guitars'

const initialState = []

const guitarReducer = (state = initialState, action) => {
  if (action.type === definitions.ADD_GUITAR) {
    return [...state, action.guitar]
  } else if (action.type === definitions.DELETE_GUITAR) {
    return state.filter((item) => {
      if (item.id != action.id) {
        return true
      }
      return false
    })
  }
  return state
}

export default guitarReducer