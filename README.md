# React Refresher

This project simply serves as a small showcase, as well as knowledge refresher
for React, it contains an App which simply lets you add or Remove Guitars.

# Installing dependencies

Simply run `npm install`

# To Run

Simply run `npm start`, The app should be available at [http://localhost:1234]([http://localhost:1234)